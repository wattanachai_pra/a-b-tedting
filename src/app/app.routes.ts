import { provideRouter, RouterConfig } from '@angular/router';
import { PageAComponent } from './components/page-a/page-a.component';
import { PageBComponent } from './components/page-b/page-b.component';


export const routes: RouterConfig = [
    { path: '', component: PageAComponent },
    { path: 'page-a', component: PageAComponent },
    { path: 'page-b', component: PageBComponent }

];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];


