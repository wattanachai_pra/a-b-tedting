import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/common';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, FormBuilder } from '@angular/forms';
@Component({
  moduleId: module.id,
  selector: 'app-page-b',
  templateUrl: 'page-b.component.html',
  directives: [REACTIVE_FORM_DIRECTIVES],
  styleUrls: ['page-b.component.css']
})
export class PageBComponent implements OnInit {
  public myForm: FormGroup;
    public submitted: boolean;
    public events: any[] = [];

    constructor(private _fb: FormBuilder) { }

    ngOnInit() {
        // the long way
        // this.myForm = new FormGroup({
        //     name: new FormControl('', [<any>Validators.required, <any>Validators.minLength(5)]),
        //     address: new FormGroup({
        //         address1: new FormControl('', <any>Validators.required),
        //         postcode: new FormControl('8000')
        //     })
        // });

        // the short way
        this.myForm = this._fb.group({
            name: ['', [<any>Validators.required, <any>Validators.minLength(5)]],
            address: this._fb.group({
                street: ['', <any>Validators.required],
                postcode: ['', <any>Validators.required]
            })
        });

        // subscribe to form changes  
        // this.subcribeToFormChanges();

        // (<FormControl>this.myForm.controls['name'])
        //     .updateValue('John', { onlySelf: true });

    }
// subcribeToFormChanges() {
//         const myFormStatusChanges$ = this.myForm.statusChanges;
//         const myFormValueChanges$ = this.myForm.valueChanges;
        
//         myFormStatusChanges$.subscribe(x => this.events.push({ event: 'STATUS_CHANGED', object: x }));
//         myFormValueChanges$.subscribe(x => this.events.push({ event: 'VALUE_CHANGED', object: x }));
//     }

    save(model: User, isValid: boolean) {
        this.submitted = true;
        console.log(model, isValid);
         alert('onSubmit called; formGroup.valid = ' + this.myForm.valid);
    }
}


export interface User {
  name: string; // required with minimum 5 chracters
  address?: {
    street?: string; // required
    postcode?: string;
  }
}