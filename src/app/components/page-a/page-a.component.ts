import { Component, OnInit } from '@angular/core';
import { Validators } from '@angular/common';
import { REACTIVE_FORM_DIRECTIVES, FormGroup, FormControl, FormBuilder } from '@angular/forms';
@Component({
  moduleId: module.id,
  selector: 'app-page-a',
  templateUrl: 'page-a.component.html',
  directives: [REACTIVE_FORM_DIRECTIVES],
  styleUrls: ['page-a.component.css']
})
export class PageAComponent implements OnInit {
   model: Hero;  
  powers: string[];
  submitted: boolean = false;
  
  constructor() { }
  
  ngOnInit() {
      this.model = new Hero(18, 'Tornado', 'Turbulent Breeze', 'Willie Wind');

      this.powers = ['Really Smart', 'Turbulent Breeze', 
                     'Super Hot', 'Weather Changer'];
  }

  onSubmit() {
    this.submitted = true;
    console.log(this.model);
    
  }
}

export class Hero {

  constructor(
    public id?: number,
    public name?: string,
    public power?: string,
    public alterEgo?: string,
    public email?: string) {

  }

}