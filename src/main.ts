import { bootstrap } from '@angular/platform-browser-dynamic';
import { enableProdMode } from '@angular/core';
import { AppComponent, environment } from './app/';
import { APP_ROUTER_PROVIDERS } from './app/app.routes';
import {HTTP_PROVIDERS} from '@angular/http';
import { disableDeprecatedForms, provideForms } from '@angular/forms';
import 'rxjs/Rx';


if (environment.production) {
  enableProdMode();
}

bootstrap(AppComponent, [
    disableDeprecatedForms(), // disable deprecated forms
  provideForms(), // enable new forms module
  APP_ROUTER_PROVIDERS,
  HTTP_PROVIDERS]).catch(err => console.error(err));

