import { ABTestingPage } from './app.po';

describe('a-b-testing App', function() {
  let page: ABTestingPage;

  beforeEach(() => {
    page = new ABTestingPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
